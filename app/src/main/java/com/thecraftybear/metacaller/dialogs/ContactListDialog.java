package com.thecraftybear.metacaller.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.thecraftybear.metacaller.ContactManager;
import com.thecraftybear.metacaller.MainActivity;
import com.thecraftybear.metacaller.PatternManager;
import com.thecraftybear.metacaller.R;
import com.thecraftybear.metacaller.adapters.ContactListViewAdapter;
import com.thecraftybear.metacaller.utils.CommonUtils;

/**
 * Created by Fahim on 6/16/2016.
 */

public class ContactListDialog extends DialogFragment {

    private MainActivity activity;
    private ListView listView;
    private PatternManager patternManager;
    private ContactManager contactManager;
    private ContactListViewAdapter contactListController;
    private ImageButton searchClearBtn;
    private EditText searchView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(MainActivity.DARK_THEME ? R.layout.contact_list_dialog_dark : R.layout.contact_list_dialog, null);

        this.searchView = (EditText) view.findViewById(R.id.searchTextView2);
        this.searchClearBtn = (ImageButton) view.findViewById(R.id.searchClearBtn2);
        searchClearBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(searchView.getText().length() > 0)
                    searchView.setText("");
                else{
                    searchView.requestFocus();
                    InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(searchView, InputMethodManager.SHOW_IMPLICIT);
                }
            }
        });

        listView = (ListView) view.findViewById(R.id.dialogListView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                activity.setPatternFromDialog(contactListController.getItem(position));
            }
        });

        return view;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().setWindowAnimations(R.style.PopupAnimation);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        this.activity = (MainActivity) getActivity();

        this.contactListController = new ContactListViewAdapter(activity, contactManager.getContacts(), searchView, searchClearBtn);
        contactManager.setListController(contactListController);
        listView.setAdapter(contactListController);
    }

    public void setExtraVars(MainActivity activity, ContactManager contactManager, PatternManager patternManager){
        this.activity = activity;
        this.contactManager = contactManager;
        this.patternManager = patternManager;
    }
}
