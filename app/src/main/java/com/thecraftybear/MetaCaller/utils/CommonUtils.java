package com.thecraftybear.metacaller.utils;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.thecraftybear.metacaller.R;

/**
 * Created by Fahim on 5/11/2016.
 */

public class CommonUtils {
    public static SharedPreferences getPreferences(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static SharedPreferences.Editor getPreferencesEditor(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).edit();
    }

    public static boolean getDarkThemeResult(Context context){
        SharedPreferences preferences = CommonUtils.getPreferences(context);
        return preferences.getBoolean(context.getString(R.string.themeKey), false);
    }

    public static String getEmail(Context context) {
        AccountManager accountManager = AccountManager.get(context);
        Account account = getAccount(accountManager);

        if (account == null) {
            return null;
        } else {
            return account.name;
        }
    }

    public static Account getAccount(AccountManager accountManager) {
        Account[] accounts = accountManager.getAccountsByType("com.google");
        Account account;
        if (accounts.length > 0) {
            account = accounts[0];
        } else {
            account = null;
        }
        return account;
    }
}