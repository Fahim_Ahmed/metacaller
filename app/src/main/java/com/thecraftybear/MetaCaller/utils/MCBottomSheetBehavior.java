package com.thecraftybear.metacaller.utils;

import android.content.Context;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.ViewDragHelper;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.thecraftybear.metacaller.R;

/**
 * Created with IntelliJ IDEA.
 * User: Fahim
 * Date: 25/5/2016
 * Time: 12:55 AM
 */
public class MCBottomSheetBehavior<V extends View> extends BottomSheetBehavior<V> {
    private boolean mAllowUserDragging = true;
    private View capturedView;
    private ViewDragHelper helper;
    private View bottomSheet;
    /**
     * Default constructor for instantiating BottomSheetBehaviors.
     */
    public MCBottomSheetBehavior(){ super(); }

    /**
     * Default constructor for inflating BottomSheetBehaviors from layout.
     *
     * @param context The {@link Context}.
     * @param attrs   The {@link AttributeSet}.
     */
    public MCBottomSheetBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setBottomSheet(View bottomSheet) {
        this.bottomSheet = bottomSheet;
    }

    public void setAllowUserDragging(boolean allowUserDragging) {
        mAllowUserDragging = allowUserDragging;
    }

    @Override
    public boolean onInterceptTouchEvent(CoordinatorLayout parent, V child, MotionEvent event) {
        return super.onInterceptTouchEvent(parent, child, event) && mAllowUserDragging;
    }
}
