package com.thecraftybear.metacaller.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;

import com.github.florent37.materialtextfield.MaterialTextField;
import com.thecraftybear.metacaller.R;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created with IntelliJ IDEA.
 * User: Fahim
 * Date: 27/5/2016
 * Time: 8:00 PM
 */
public class InputTextDialog extends DialogFragment implements View.OnClickListener {

    private PatternRecordDialog rootDialog;
    private EditText inputView;

    public void setRootDialog(PatternRecordDialog rootDialog) {
        this.rootDialog = rootDialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.name_input_dialog, null);

        View doneBtn = view.findViewById(R.id.setBtn);
        doneBtn.setOnClickListener(this);

        View closeBtn = view.findViewById(R.id.cancelBtn);
        closeBtn.setOnClickListener(this);

        //MaterialTextField materialTextField = (MaterialTextField) view.findViewById(R.id.materialTextView);
        inputView = (EditText) view.findViewById(R.id.nameInputView);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if(getDialog() == null) return;
        getDialog().getWindow().setWindowAnimations(R.style.PopupAnimation);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().setWindowAnimations(R.style.PopupAnimation);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.setBtn:
                String str = inputView.getText().toString();
                if(str.trim().isEmpty() || str.length() < 1)
                    showResult(true, "Invalid name.");
                else{
                    rootDialog.setName(str);
                    dismiss();
                }
                break;
            case R.id.cancelBtn:
                dismiss();
                break;
            default:
                break;
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        inputView.setText("");
    }

    private void showResult(boolean isError, String msg){
        if(!isError) {
            final SweetAlertDialog pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.SUCCESS_TYPE);
            pDialog.setTitleText("Success");
            pDialog.setContentText(msg);
            pDialog.setCancelable(false);
            pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    pDialog.dismiss();
                    dismiss();
                }
            });
            pDialog.show();
        }else{
            final SweetAlertDialog pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE);
            pDialog.setTitleText("Error");
            pDialog.setContentText(msg);
            pDialog.setCancelable(false);
            pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    pDialog.dismiss();
                }
            });
            pDialog.show();
        }
    }
}
