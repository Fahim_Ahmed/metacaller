package com.thecraftybear.metacaller.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.thecraftybear.metacaller.MainActivity;
import com.thecraftybear.metacaller.PatternManager;
import com.thecraftybear.metacaller.Lock9View;
import com.thecraftybear.metacaller.R;
import com.thecraftybear.metacaller.data.PatternNode;
import com.thecraftybear.metacaller.interfaces.Communicator;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.codetail.animation.SupportAnimator;
import io.codetail.animation.ViewAnimationUtils;

/**
 * Created with IntelliJ IDEA.
 * User: Fahim
 * Date: 11/5/2016
 * Time: 9:54 PM
 */
public class PatternRecordDialog extends DialogFragment implements View.OnClickListener{

    private PatternManager patternManager;
    private Communicator communicator;
    private String patternCode = "";
    private PatternNode selectedContact;
    private boolean editMode = false;
    private Lock9View lock9View;
    private boolean isCustomNumber;
    private InputTextDialog inputTextDialog;
    public RelativeLayout rootView;

    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(MainActivity.DARK_THEME ? R.layout.pattern_add_dialog_dark : R.layout.pattern_add_dialog, null);

        View doneBtn = view.findViewById(R.id.doneBtn);
        doneBtn.setOnClickListener(this);

        View closeBtn = view.findViewById(R.id.closeBtn);
        closeBtn.setOnClickListener(this);

        rootView = (RelativeLayout) view.findViewById(R.id.fragmentRoot);

        lock9View = (Lock9View) view.findViewById(R.id.lock_9_view);
        lock9View.setCallBack(new Lock9View.CallBack(){
            @Override
            public void onFinish(String password){
                patternCode = password;
                if(patternCode.equalsIgnoreCase("-1")) patternCode = "";
            }

            @Override
            public void onDrawPattern(String password){
                patternCode = password;
                if(patternCode.equalsIgnoreCase("-1")) patternCode = "";
            }

            @Override
            public void onDrawStart() {

            }

            @Override
            public void onDrawEnd() {

            }
        });

        return view;
    }

//    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().setWindowAnimations(R.style.PopupAnimation);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//
//        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
//            @Override
//            public void onShow(DialogInterface dialog) {
//                reveal(rootView);
//            }
//        });

        return dialog;
    }

    void reveal(View view){
        // get the center for the clipping circle
        int cx = (view.getLeft() + view.getRight()) / 2;
        int cy = (view.getTop() + view.getBottom()) / 2;

        // get the final radius for the clipping circle
        int dx = Math.max(cx, view.getWidth() - cx);
        int dy = Math.max(cy, view.getHeight() - cy);
        //int dx = Math.max(cx, view.getWidth());
        //int dy = Math.max(cy, view.getHeight());
        float finalRadius = (float) Math.hypot(dx, dy);

        // Android native animator
        //SupportAnimator animator = ViewAnimationUtils.createCircularReveal(view, view.getRight(), view.getBottom(), 0, finalRadius);
        SupportAnimator animator = ViewAnimationUtils.createCircularReveal(view, cx, cy, 0, finalRadius);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.setDuration(1200);
        animator.start();
    }

//    @Override
//    public void onStart() {
//        super.onStart();
//        if(getDialog() == null) return;
//        //getDialog().getWindow().setWindowAnimations(R.style.PopupAnimation);
//    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        communicator = (Communicator) activity;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        lock9View.setPattern(selectedContact.getPatternCode());
        //patternCode = selectedContact.getPatternCode();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.doneBtn:
                if(patternManager.isExist(patternCode))
                    showResult(true, "Duplicate pattern.");
                else if(patternCode.length() < 2){
                    showResult(true, "Invalid pattern.");
                } else{
                    if(editMode){
                        showConfirmation();
                    }else if(isCustomNumber){
                        inputTextDialog.show(getFragmentManager(), "inputFragment");
                    }else {
                        selectedContact.setPatternCode(patternCode);
                        patternManager.addPatternNode(selectedContact);
                        communicator.onSuccess();

                        showResult(false, "Pattern added.");
                        patternCode = "";
                    }
                }
                break;
            case R.id.closeBtn:
                dismiss();
                break;
            default:
                break;
        }
    }

    public void show(PatternNode node, FragmentManager manager, String tag){
        this.show(manager, tag);
        this.selectedContact = node;

        if(patternManager.isExist(selectedContact.getPatternCode())) {
            editMode = true;
        }

        if(node.getContactName().length() < 1){
            isCustomNumber = true;
        }
    }

    public void setName(String name){
        selectedContact.setContactName(name);
        selectedContact.setPatternCode(patternCode);
        patternManager.addPatternNode(selectedContact);

        showResult(false, "Pattern added.");
    }

    public void showResult(boolean isError, String msg){
        if(!isError) {
            final SweetAlertDialog pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.SUCCESS_TYPE);
            pDialog.setTitleText("Success");
            pDialog.setContentText(msg);
            pDialog.setCancelable(false);
            pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    pDialog.dismiss();
                    dismiss();
                }
            });
            pDialog.show();
        }else{
            final SweetAlertDialog pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE);
            pDialog.setTitleText("Error");
            pDialog.setContentText(msg);
            pDialog.setCancelable(false);
            pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    pDialog.dismiss();
                }
            });
            pDialog.show();
        }
    }

    private void showConfirmation(){
        final SweetAlertDialog pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE);
        pDialog.setTitleText("Are you sure?");
        pDialog.setContentText("Overwrite new pattern?");
        pDialog.setCancelable(true);
        pDialog.setCancelText("Cancel");
        pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                pDialog.cancel();
            }
        });
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                pDialog.dismiss();

                PatternNode node = selectedContact;
                String oldCode = node.getPatternCode();
                node.setPatternCode(patternCode);
                patternManager.replaceNode(node, oldCode);
                communicator.onRespond(oldCode, patternCode);

                showResult(false, "New pattern saved.");
                patternCode = "";
                editMode = false;
            }
        });
        pDialog.show();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        editMode = false;
        isCustomNumber = false;
        patternCode = "";
    }

    public void setManager(PatternManager patternManager) {
        this.patternManager = patternManager;

        inputTextDialog = new InputTextDialog();
        inputTextDialog.setRootDialog(this);
    }
}
