package com.thecraftybear.metacaller;

import android.content.Context;
import android.os.Environment;
import android.supportModified.v4.util.ArrayMap;

import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.thecraftybear.metacaller.adapters.BottomSheetListViewAdapter;
import com.thecraftybear.metacaller.data.PatternNode;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created with IntelliJ IDEA.
 * User: Fahim
 * Date: 10/5/2016
 * Time: 7:36 PM
 */
public class PatternManager{

    private ArrayMap<String, PatternNode> patternList;
    private Context context;
    private BottomSheetListViewAdapter listController;
    private SwipeMenuListView listView;

    public PatternManager(Context context){
        this.context = context;
        patternList = readPatternData();
    }

    public void setPatternListController(BottomSheetListViewAdapter listController){
        this.listController = listController;
    }

    public void addPatternNode(PatternNode node){
        String key = node.getPatternCode();
        patternList.put(key, node);
        listController.addNewContactKey(key);
        updateListView();
        savePatternData();
    }

    public void replaceNode(PatternNode newNode, String oldCode){
        patternList.remove(oldCode);
        patternList.put(newNode.getPatternCode(), newNode);
        listController.replaceEditedCode(newNode.getPatternCode(), oldCode);
        //updateListView();
        savePatternData();
    }

    public PatternNode hasThisNode(String number){
        int len = patternList.size();
        for (int i = 0; i < len; i++) {
            PatternNode n = patternList.valueAt(i);
            if (n.getContactNumber().equalsIgnoreCase(number))
                return n;
        }

        return null;
    }

    public PatternNode containThisNumber(String number){
        int len = patternList.size();
        for (int i = 0; i < len; i++) {
            PatternNode n = patternList.valueAt(i);
            if (n.getContactNumber().contains(number))
                return n;
        }

        return null;
    }

    public void updateListView(){
        listController.notifyDataSetChanged();
    }

    public void addPatternNode(String id, String code, String number, String name){
        PatternNode node = new PatternNode(id, code, number, name);
        String key = node.getPatternCode();
        patternList.put(key, node);
        updateListView();
        savePatternData();
    }

    public boolean isExist(String code){
        return patternList.containsKey(code);
    }

    public void removeNode(String code){
        listController.removeKey(code);
        patternList.remove(code);
        updateListView();
        savePatternData();
    }

    //save to disk (wow, i'm writing comments :/ )
    public boolean savePatternData(){
        if(patternList == null) return false;

        try {
            String root = Environment.getExternalStorageDirectory().toString();
            File file = new File(root + File.separator + "MetaCaller");
            if(!file.exists()) file.mkdir();

            file = new File(file, context.getResources().getString(R.string.pattern_file_name));
            FileOutputStream fos = new FileOutputStream(file);
            //FileOutputStream fos = new FileOutputStream(context.getFilesDir() + File.separator + context.getResources().getString(R.string.pattern_file_name));
            ObjectOutputStream out = new ObjectOutputStream(fos);

            out.writeObject(patternList);
            out.flush();
            out.close();
            System.out.println(" --> " +"NOT FAIL");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println(" --> " +"FAIL");
            return false;
        }

        System.out.println("-----> " + "Pattern Saved");

        return true;
    }

    public int getSize(){ return patternList.size(); }

    public boolean isEmpty(){
        return patternList.isEmpty();
    }

    //read from disk if null
    public ArrayMap<String, PatternNode> readPatternData(){

        if(patternList == null){
            patternList = new ArrayMap<>();

            try {
                File file = new File(Environment.getExternalStorageDirectory().toString() + File.separator + "MetaCaller"
                        + File.separator + context.getResources().getString(R.string.pattern_file_name));
                if(!file.exists()) return patternList;

                FileInputStream fos = new FileInputStream(file);
                ObjectInputStream oi = new ObjectInputStream(fos);

                patternList = (ArrayMap<String, PatternNode>) oi.readObject();

                System.out.println("-----> " + "Read from disk.");

                oi.close();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
                System.out.println(" --> " +"READ FAIL");
            }
        }

        return patternList;
    }

    public PatternNode getNode(int index){
        return patternList.valueAt(index);
    }

    public PatternNode getNode(String code) {
        return patternList.get(code);
    }

    public void setListView(SwipeMenuListView listView) { this.listView = listView; }

    public void cleanData() {
        String root = Environment.getExternalStorageDirectory().toString();
        File file = new File(root + File.separator + "MetaCaller" + File.separator
                + context.getResources().getString(R.string.pattern_file_name));

        if(file.exists()){
            file.delete();
        }

        patternList.clear();
        listController.clearData();
    }
}
