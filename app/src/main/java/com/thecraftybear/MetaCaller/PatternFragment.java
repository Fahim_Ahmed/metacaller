package com.thecraftybear.metacaller;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thecraftybear.metacaller.interfaces.Communicator;

/**
 * Created with IntelliJ IDEA.
 * User: Fahim
 * Date: 8/5/2016
 * Time: 10:09 PM
 */
public class PatternFragment extends Fragment {
    Communicator communicator;
    PatternManager patternManager;
    private Lock9View lock9View;
    public boolean isDrawing = false;
    public int tapNum;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //container = (RevealFrameLayout) view.findViewById(R.id.fragmentRoot);
        return inflater.inflate(MainActivity.DARK_THEME ? R.layout.pattern_list_layout_dark : R.layout.pattern_list_layout, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        communicator = (Communicator) getActivity();

        lock9View = (Lock9View) getActivity().findViewById(R.id.lock_9_view);
        lock9View.setCallBack(new Lock9View.CallBack() {
            @Override
            public void onFinish(String password) { communicator.onDrawPattern(password); }

            @Override
            public void onDrawPattern(String password) {
                communicator.onDrawPattern(password);
            }

            @Override
            public void onDrawStart() {
                isDrawing = true;
            }

            @Override
            public void onDrawEnd() {
                isDrawing = false;
            }
        });

        lock9View.setTouchable(true);
    }

    public void setPattern(String pat){ lock9View.setPattern(pat); }
    public void clearPattern(){ lock9View.clearPattern(); }
    public void setManager(PatternManager patternManager){
        this.patternManager = patternManager;
    }
    public void setVisibility(boolean state){ lock9View.setVisibility(state ? View.VISIBLE : View.INVISIBLE); }
    public void setClickable(boolean state){ lock9View.setClickable(state); }
    public void setFocusable(boolean state){ lock9View.setFocusable(state); }
    public void setTouchable(boolean state){ lock9View.setTouchable(state); }
}
