package com.thecraftybear.metacaller.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thecraftybear.metacaller.Lock9View;
import com.thecraftybear.metacaller.R;
import com.thecraftybear.metacaller.data.PatternNode;
import com.thecraftybear.metacaller.utils.CommonUtils;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Fahim
 * Date: 13/5/2016
 * Time: 12:41 AM
 */
public class ContactListViewAdapter extends BaseAdapter implements Filterable {
    private ArrayList<PatternNode> contacts;
    private final Context context;
    private final EditText searchView;
    private ArrayList<PatternNode> mStringFilterList;
    private ValueFilter valueFilter;
    private ImageButton clearBtn;
    private boolean darkTheme;

    public ContactListViewAdapter(final Context context, final ArrayList<PatternNode> contacts, final EditText searchView, final ImageButton clearBtn) {
        this.context = context;
        this.contacts = contacts;
        this.mStringFilterList = contacts;
        this.searchView = searchView;
        this.clearBtn = clearBtn;

        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                getFilter().filter(s);

                clearBtn.setImageDrawable(searchView.getText().length() > 0 ?
                        ContextCompat.getDrawable(context, R.mipmap.close_dark) :
                        ContextCompat.getDrawable(context, R.mipmap.search_black));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        darkTheme = CommonUtils.getDarkThemeResult(context);
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        return (getItem(position).getPatternCode().length() == 0) ? 0 : 1;
    }

    @Override
    public int getCount() {
        return contacts.size();
    }

    @Override
    public PatternNode getItem(int position) {
        return contacts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = View.inflate(context, (darkTheme) ? R.layout.contact_list_row_layout_dark : R.layout.contact_list_row_layout, null);
            new ViewHolder(convertView);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        //ApplicationInfo item = getItem(position);

        PatternNode node = getItem(position);
        holder.nameView.setText(node.getContactName());
        holder.numView.setText(node.getContactNumber());

        boolean lenZero = (node.getPatternCode().length() == 0);
        holder.linkView.setVisibility(lenZero ? View.INVISIBLE : View.VISIBLE);
        holder.lockView.setVisibility(lenZero ? View.INVISIBLE : View.VISIBLE);
        if(!lenZero) holder.lockView.setPattern(node.getPatternCode());

        if(darkTheme){
            Drawable val = ContextCompat.getDrawable(context, (position % 2 == 0) ? R.drawable.row_selector_darkblue_10 : R.drawable.row_selector_darkblue_8);
            holder.rowBackView.setBackground(val);
        }else {
            Drawable val = ContextCompat.getDrawable(context, (position % 2 == 0) ? R.drawable.row_selector_white_95 : R.drawable.row_selector_white_92);
            holder.rowBackView.setBackground(val);
        }

        return convertView;
    }

    @Override
    public Filter getFilter() {
        if(valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    class ViewHolder {
        private final TextView nameView;
        private final TextView numView;
        private final ImageView linkView;
        private final RelativeLayout rowBackView;
        private final Lock9View patternView;
        private final Lock9View lockView;

        public ViewHolder(View view) {
            nameView = (TextView) view.findViewById(R.id.contactNameView);
            numView = (TextView) view.findViewById(R.id.contactNumView);
            rowBackView = (RelativeLayout) view.findViewById(R.id.rowView);
            linkView = (ImageView) view.findViewById(R.id.patternTicker);
            lockView = (Lock9View) view.findViewById(R.id.lockViewMini);
            patternView = (Lock9View) view.findViewById(R.id.lockViewMini);
            patternView.setTouchable(false);
            patternView.setVisibility(View.GONE);

            view.setTag(this);
        }
    }

    private class ValueFilter extends Filter {

        //Invoked in a worker thread to filter the data according to the constraint.
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if(constraint != null && constraint.length() > 0){

                ArrayList<PatternNode> filteredList = new ArrayList<>();
                int len = mStringFilterList.size();

                for(int i=0; i < len; i++){
                    if((mStringFilterList.get(i).getContactName().toUpperCase()).contains(constraint.toString().toUpperCase())) {
                        PatternNode node = new PatternNode();
                        node.setContactName(mStringFilterList.get(i).getContactName());
                        node.setPatternCode(mStringFilterList.get(i).getPatternCode());
                        node.setContactNumber(mStringFilterList.get(i).getContactNumber());

                        filteredList.add(node);
                    }
                }

                results.count = filteredList.size();
                results.values = filteredList;

            }else{
                results.count = mStringFilterList.size();
                results.values = mStringFilterList;
            }

            return results;
        }


        //Invoked in the UI thread to publish the filtering results in the user interface.
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            contacts = (ArrayList<PatternNode>) results.values;
            notifyDataSetChanged();
        }
    }
}
