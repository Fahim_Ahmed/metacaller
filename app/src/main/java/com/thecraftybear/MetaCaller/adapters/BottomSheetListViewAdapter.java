package com.thecraftybear.metacaller.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.supportModified.v4.util.ArrayMap;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thecraftybear.metacaller.Lock9View;
import com.thecraftybear.metacaller.PatternManager;
import com.thecraftybear.metacaller.R;
import com.thecraftybear.metacaller.data.PatternNode;
import com.thecraftybear.metacaller.interfaces.Communicator;
import com.thecraftybear.metacaller.utils.CommonUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created with IntelliJ IDEA.
 * User: Fahim
 * Date: 14/5/2016
 * Time: 8:29 PM
 */
public class BottomSheetListViewAdapter extends BaseAdapter implements Filterable{
    private final PatternManager patternManager;
    private final Button addBtn;
    private final boolean darkTheme;

    // This bhonita is for sorted keys -_-
    private ArrayList<String> contactsKeys;
    private ArrayList<String> contactsFiltered;

    private final Context context;
    private final TextView countView;
    private final TextView emptyView;
    private ValueFilter valueFilter;
    private Communicator comm;

    public int editedIndex = -1;
    private String currentPass;

    public BottomSheetListViewAdapter(Context context, ArrayMap<String, PatternNode> contacts, TextView countView, TextView emptyView, Button patternButtonView, PatternManager patternManager) {
        this.context = context;
        this.countView = countView;
        this.emptyView = emptyView;
        this.addBtn = patternButtonView;
        this.patternManager = patternManager;

        this.contactsKeys = new ArrayList<>();

        int len = contacts.size();
        for (int i = 0; i < len; i++){
            this.contactsKeys.add(contacts.valueAt(i).getPatternCode());
        }

        sortContactKeys();

        this.contactsFiltered = this.contactsKeys;
        this.comm = (Communicator) context;

        this.currentPass = "";

        darkTheme = CommonUtils.getDarkThemeResult(context);
    }

    public int hasThisKey(String key){
        int len = contactsKeys.size();
        for (int i = 0; i < len; i++) {
            if(key.equalsIgnoreCase(contactsKeys.get(i)))
                return i;
        }

        return -1;
    }

    public void onPatternDraw(String password){
        if(password.length() > 1)
            getFilter().filter(password);
        else getFilter().filter("");

        this.currentPass = password;
    }

    public void addNewContactKey(String key) {
        getFilter().filter("");
        notifyDataSetChanged();

        contactsKeys.add(key);

        if(currentPass.length() > 1) {
            contactsFiltered.add(key);
//            int len = contactsFiltered.size();
//            for (int i = 0; i < len; i++) {
//                if (contactsFiltered.get(i).contains(currentPass)) {
//                    contactsFiltered.add(key);
//                    break;
//                }
//            }
        }

        sortContactKeys();
    }

    public void replaceEditedCode(String code, String oldCode){
        int k = hasThisKey(oldCode);
        if(k != -1) contactsKeys.remove(k);

        int len = contactsFiltered.size();
        for (int i = 0; i < len; i++) {
            if(oldCode.equalsIgnoreCase(contactsFiltered.get(i))) {
                contactsFiltered.remove(i);
                break;
            }
        }

        contactsKeys.add(code);
        if (currentPass.length() > 1) {
            contactsFiltered.add(code);
        }

//  for (int i = 0; i < len; i++) {
//                if (contactsFiltered.get(i).contains(currentPass)) {
//                    contactsFiltered.add(code);
//                    break;
//                }
//            }
//        }

        sortContactKeys();

        notifyDataSetChanged();
    }

    public void removeKey(String key) {
        int k = hasThisKey(key);
        if(k != -1) contactsKeys.remove(k);

        int len = contactsFiltered.size();
        for (int i = 0; i < len; i++) {
            if(key.equalsIgnoreCase(contactsFiltered.get(i))) {
                contactsFiltered.remove(i);
                break;
            }
        }
    }

    public void clearData() {
        contactsKeys.clear();
        contactsFiltered.clear();
        notifyDataSetChanged();
    }

    private void sortContactKeys() {
        Collections.sort(this.contactsKeys, new Comparator<String>() {
            @Override
            public int compare(String lhs, String rhs) {
                Integer lv = Integer.parseInt(lhs);
                Integer rv = Integer.parseInt(rhs);

                return lv.compareTo(rv);
            }
        });
    }

    @Override
    public int getCount() {
        int count = contactsKeys.size();
        countView.setText((count == 1) ? "1 PATTERN" : count + " PATTERNS");
        emptyView.setVisibility((count == 0) ? View.VISIBLE : View.GONE);
        addBtn.setVisibility((count == 0 && contactsFiltered.size() == 0) ? View.VISIBLE : View.GONE);

        return count;
    }

    @Override
    public PatternNode getItem(int position) {
        return patternManager.getNode(contactsKeys.get(position)); //contactsKeys.valueAt(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = View.inflate(context, (darkTheme) ? R.layout.contact_list_row_layout_dark : R.layout.contact_list_row_layout, null);
            new ViewHolder(convertView);
        }

        ViewHolder holder = (ViewHolder) convertView.getTag();

        PatternNode node = getItem(position);
        holder.nameView.setText(node.getContactName());
        holder.numView.setText(node.getContactNumber());
        holder.patternView.setPattern(node.getPatternCode());
        holder.patternView.setTouchable(false);
        holder.ticker.setVisibility(View.GONE);

        if(darkTheme){
            Drawable val = ContextCompat.getDrawable(context, (position % 2 == 0) ? R.drawable.row_selector_darkblue_10 : R.drawable.row_selector_darkblue_8);
            holder.rowBackView.setBackground(val);
        }else {
            Drawable val = ContextCompat.getDrawable(context, (position % 2 == 0) ? R.drawable.row_selector_white_95 : R.drawable.row_selector_white_92);
            holder.rowBackView.setBackground(val);
        }

        return convertView;
    }

    @Override
    public Filter getFilter() {
        if(valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView nameView;
        private final TextView numView;
        private final RelativeLayout rowBackView;
        private final Lock9View patternView;
        private final ImageView ticker;
//        private final SwipeRevealLayout swipeLayout;

        public ViewHolder(View view){
            super(view);
            nameView = (TextView) view.findViewById(R.id.contactNameView);
            numView = (TextView) view.findViewById(R.id.contactNumView);
            rowBackView = (RelativeLayout) view.findViewById(R.id.rowView);
            ticker = (ImageView) view.findViewById(R.id.patternTicker);
            patternView = (Lock9View) view.findViewById(R.id.lockViewMini);
//            swipeLayout = (SwipeRevealLayout) view.findViewById(R.id.swipe_layout);

            view.setTag(this);
        }
    }

    private class ValueFilter extends Filter {

        //Invoked in a worker thread to filter the data according to the constraint.
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if(constraint != null && constraint.length() > 1){

                ArrayList<String> filteredList = new ArrayList<>();

                int len = contactsFiltered.size();
                for(int i=0; i < len; i++){
                    if((contactsFiltered.get(i)).contains(constraint.toString())) {
                        filteredList.add(contactsFiltered.get(i));
                    }
                }

                results.count = filteredList.size();
                results.values = filteredList;

            }else{
                results.count = contactsFiltered.size();
                results.values = contactsFiltered;
            }

            return results;
        }


        //Invoked in the UI thread to publish the filtering results in the user interface.
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            contactsKeys = (ArrayList<String>) results.values;
            notifyDataSetChanged();
            comm.onChangeFilter(0);
        }
    }
}
