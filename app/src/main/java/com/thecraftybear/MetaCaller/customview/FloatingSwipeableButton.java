package com.thecraftybear.metacaller.customview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Vibrator;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;

import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.animation.ValueAnimator;
import com.thecraftybear.metacaller.R;
import com.thecraftybear.metacaller.utils.DensityUtil;

/**
 * Created by Fahim on 5/18/2016.
 */

public class FloatingSwipeableButton extends ViewGroup implements Animator.AnimatorListener {

    public enum OVERLAPPED_STATE{
        ENTER,
        OVERLAPPED,
        EXIT
    }

    public enum SELECTION{
        CALL,
        COPY,
        MESSAGE
    }

    private Context context;
    private boolean firstTime;

    private float cx, cy;

    private float distThreshold;
    private Paint paint;
    private SwipeView mainBtn;
    private SwipeView btnBackground;
    private SwipeView hover_call;
    private SwipeView hover_msg;
    private SwipeView hover_copy;
    private SwipeView call_view;
    private SwipeView msg_view;
    private SwipeView copy_view;
    private boolean canMove;

    private Vibrator vibrator;
    private boolean enableVibrate;
    private int vibrateTime;

    Drawable CALL_ICON;
    Drawable MSG_ICON;
    Drawable COPY_ICON;

    // Communicator -------------
    private Callback callback;

    public interface Callback{
        void onRelease(SELECTION selection);
    }

    public void setCallback(Callback callback){
        this.callback = callback;
    }
    // --------------------------

    public FloatingSwipeableButton(Context context) {
        super(context);
        init(context);
    }

    public FloatingSwipeableButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public FloatingSwipeableButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

//    public FloatingSwipeableButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
//        super(context, attrs, defStyleAttr, defStyleRes);
//        init(context);
//    }

    private void init(Context context){
        this.context = context;
        this.distThreshold = (float) (DensityUtil.getScreenWidth(context) * 0.5);

        enableVibrate = true;
        vibrateTime = 20;

        if (enableVibrate && !isInEditMode()) {
            vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        }

        firstTime = true;

        setClipChildren(false);

        paint = new Paint(Paint.DITHER_FLAG);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(DensityUtil.dp2px(context, 5));
        paint.setColor(Color.parseColor("#ffd54f"));
        paint.setAntiAlias(true);

        CALL_ICON = ContextCompat.getDrawable(context, R.drawable.call_icon);
        MSG_ICON = ContextCompat.getDrawable(context, R.drawable.msg_icon);
        COPY_ICON = ContextCompat.getDrawable(context, R.drawable.copy_icon);

        btnBackground = new SwipeView(context);
        btnBackground.setBackground(ContextCompat.getDrawable(context, R.drawable.button_background));
        btnBackground.setVisibility(INVISIBLE);
        addView(btnBackground);

        call_view = new SwipeView(context);
        call_view.setBackground(CALL_ICON);
        call_view.setVisibility(INVISIBLE);
        addView(call_view);

        msg_view = new SwipeView(context);
        msg_view.setBackground(MSG_ICON);
        msg_view.setVisibility(INVISIBLE);
        addView(msg_view);

        copy_view = new SwipeView(context);
        copy_view.setBackground(COPY_ICON);
        copy_view.setVisibility(INVISIBLE);
        addView(copy_view);

        hover_call = new SwipeView(context);
        hover_call.setBackground(ContextCompat.getDrawable(context, R.drawable.hoverarea));
        hover_call.setTag("call_view");
        hover_msg = new SwipeView(context);
        hover_msg.setBackground(ContextCompat.getDrawable(context, R.drawable.hoverarea));
        hover_msg.setTag("msg_view");
        hover_copy = new SwipeView(context);
        hover_copy.setBackground(ContextCompat.getDrawable(context, R.drawable.hoverarea));
        hover_copy.setTag("copy_view");
        addView(hover_call);
        addView(hover_msg);
        addView(hover_copy);

        mainBtn = new SwipeView(context);
        addView(mainBtn);


        setWillNotDraw(false);
    }

    @Override
    protected int[] onCreateDrawableState(int extraSpace) {
        return super.onCreateDrawableState(extraSpace);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if(firstTime) return;

        float dx = mainBtn.getX() + mainBtn.getWidth()/2;
        float dy = mainBtn.getY() + mainBtn.getHeight()/2;

        canvas.drawLine(cx + mainBtn.getWidth()/2, cy + mainBtn.getHeight()/2, dx, dy, paint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(firstTime) {
            firstTime = false;
            cx = mainBtn.getCenterX() - mainBtn.getWidth()/2;
            cy = mainBtn.getCenterY() - mainBtn.getHeight()/2;
            Log.i("cx", cx + " " + cy);

            float ratio = (getWidth() > getHeight()) ? getWidth() / getHeight() : getHeight() / getWidth();
            System.out.println("raito: " + ratio);
        }

        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                ObjectAnimator.ofFloat(mainBtn, "translationX", 0)
                        .setDuration(150)
                        .start();

                ObjectAnimator oa = ObjectAnimator.ofFloat(mainBtn, "translationY", 0);
                oa.setDuration(150);
                oa.start();

                oa.addListener(this);
                oa.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        invalidate();
                    }
                });

                if(hover_call.STATE == OVERLAPPED_STATE.OVERLAPPED) callback.onRelease(SELECTION.CALL);
                else if(hover_msg.STATE == OVERLAPPED_STATE.OVERLAPPED) callback.onRelease(SELECTION.MESSAGE);
                else if(hover_copy.STATE == OVERLAPPED_STATE.OVERLAPPED) callback.onRelease(SELECTION.COPY);

                canMove = false;
                break;
            case MotionEvent.ACTION_DOWN:
                if(isOnView(mainBtn, event.getX(), event.getY())){
                    mainBtn.setFocusedState();
                    btnBackground.setVisibility(VISIBLE);
                    canMove = true;
                    btnBackground.triggerAnim(R.anim.button_trigger, true);
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if(!canMove) break;

                btnBackground.setVisibility(VISIBLE);

                // touch point
                float tx = event.getX();
                float ty = event.getY();

                // view position + center offset
                float centerX = (cx + mainBtn.getWidth() * 0.5f);
                float centerY = (cy + mainBtn.getHeight() * 0.5f);

                // angle between view and touch
                float theta = (float) Math.atan2((centerY - ty), (tx - centerX));

                // max distance threshold
                //float dist = (float) Math.sqrt(Math.pow(Math.abs(tx - (cx + view.getWidth()*0.5)), 2) + Math.pow(Math.abs(ty - (cy + view.getHeight())), 2));
                float dist = (float) Math.sqrt(Math.pow(Math.abs(tx - centerX), 2) + Math.pow(Math.abs(ty - centerY), 2));
                if(dist >= distThreshold) dist = distThreshold;

                // new location point
                float sx = (float) (Math.cos(theta) * dist + cx);
                float sy = (float) (Math.sin(theta) * dist * -1 + cy);

                isOverlapped(hover_call);
                isOverlapped(hover_msg);
                isOverlapped(hover_copy);

                //System.out.println("-----> " + tx + " " + ty);

                mainBtn.animate()
                        .x(sx)
                        .y(sy)
                        .setDuration(0)
                        .start();

                invalidate();
                break;
        }

        return true;
    }

    private boolean isOnView(View view, float x, float y) {
        return (x >= view.getLeft() && x <= view.getRight() &&
                y >= view.getTop() && y <= view.getBottom());
    }

    private boolean isOverlapped(SwipeView targetView){
        float dx = mainBtn.getX() + mainBtn.getWidth()/2;
        float dy = mainBtn.getY() + mainBtn.getHeight()/2;

        float tx = targetView.getCenterX();
        float ty = targetView.getCenterY();

        int dist = (int) Math.sqrt(Math.pow(Math.abs(tx-dx), 2) +  Math.pow(Math.abs(ty-dy), 2));

        if(dist < mainBtn.getWidth()/2){
            if(targetView.STATE == OVERLAPPED_STATE.ENTER) {
                targetView.STATE = OVERLAPPED_STATE.OVERLAPPED;

                vibrator.vibrate(vibrateTime);

                if(targetView.getTag() == "call_view"){
                    call_view.setVisibility(VISIBLE);
                }else if(targetView.getTag() == "msg_view"){
                    msg_view.setVisibility(VISIBLE);
                }else if(targetView.getTag() == "copy_view"){
                    copy_view.setVisibility(VISIBLE);
                }
            }

            if(targetView.STATE == OVERLAPPED_STATE.EXIT)
                targetView.STATE = OVERLAPPED_STATE.ENTER;

            return true;
        }else {
            targetView.STATE = OVERLAPPED_STATE.EXIT;

            if(targetView.getTag() == "call_view"){
                call_view.setVisibility(INVISIBLE);
            }else if(targetView.getTag() == "msg_view"){
                msg_view.setVisibility(INVISIBLE);
            }else if(targetView.getTag() == "copy_view"){
                copy_view.setVisibility(INVISIBLE);
            }
        }

        return false;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Drawable d = btnBackground.getBackground();

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int dw = d.getIntrinsicWidth();
        int dh = d.getIntrinsicHeight();

        if(dw == 0) dw = DensityUtil.dp2px(context, 20);
        if(dh == 0) dw = DensityUtil.dp2px(context, 20);

        //Measure Width
        int width, height;

        if (widthMode == MeasureSpec.EXACTLY) {
            //Must be this size
            width = widthSize;
        } else if (widthMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            width = Math.min(dw, widthSize);
        } else {
            //Be whatever you want
            width = dw;
        }

        //Measure Height
        if (heightMode == MeasureSpec.EXACTLY) {
            //Must be this size
            height = heightSize;
        } else if (heightMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            height = Math.min(dh, heightSize);
        } else {
            //Be whatever you want
            height = dh;
        }

        setMeasuredDimension(width, height);
    }


    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        if(changed){
            int areaWidth = r-l;
            int areaHeight = b-t;

            int btnW = DensityUtil.dp2px(context, 62);
            int btnH = DensityUtil.dp2px(context, 62);

            int ml = (areaWidth - btnW) / 2;
            int mr = ml + btnW;
            int mt = (areaHeight - btnH) / 2;
            int mb = mt + btnH;
            mainBtn.layout(ml, mt, mr, mb);

            btnW = DensityUtil.dp2px(context, 72);
            btnH = DensityUtil.dp2px(context, 72);

            ml = (areaWidth - btnW) / 2;
            mr = ml + btnW;
            mt = (areaHeight - btnH) / 2;
            mb = mt + btnH;
            btnBackground.layout(ml, mt, mr, mb);

            btnW = DensityUtil.dp2px(context, 24);
            btnH = DensityUtil.dp2px(context, 24);

            ml = (areaWidth - btnW) / 2;
            mr = ml + btnW;
            mt = (areaHeight - btnH) / 2;
            mb = mt + btnH;
            call_view.layout(ml, mt, mr, mb);
            msg_view.layout(ml, mt, mr, mb);
            copy_view.layout(ml, mt, mr, mb);

            btnW = DensityUtil.dp2px(context, 12);
            btnH = DensityUtil.dp2px(context, 12);

            ml = areaWidth/4 - btnW;
            mr = ml + btnW;
            mt = (areaHeight - btnH) / 2;
            mb = mt + btnH;
            hover_msg.layout(ml, mt, mr, mb);

            ml = 3*areaWidth/4 + btnW/2;
            mr = ml + btnW;
            mt = (areaHeight - btnH) / 2;
            mb = mt + btnH;
            hover_call.layout(ml, mt, mr, mb);

            ml = (areaWidth - btnW) / 2;
            mr = ml + btnW;
            mt = 0;
            mb = mt + btnH;
            hover_copy.layout(ml, mt, mr, mb);
        }
    }

    @Override
    public void onAnimationStart(Animator animation) {

    }

    @Override
    public void onAnimationEnd(Animator animation) {
        btnBackground.setVisibility(INVISIBLE);
        mainBtn.setDefaultState();
    }

    @Override
    public void onAnimationCancel(Animator animation) {

    }

    @Override
    public void onAnimationRepeat(Animator animation) {

    }

    class SwipeView extends View {
        public OVERLAPPED_STATE STATE = OVERLAPPED_STATE.EXIT;

        public SwipeView(Context context) {
            super(context);
            setBackground(ContextCompat.getDrawable(context, R.drawable.buttonnormalstate));
            setClipChildren(false);
        }

        public SwipeView(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public SwipeView(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
        }

        public void triggerAnim(int animId, boolean vibrate){
            startAnimation(AnimationUtils.loadAnimation(getContext(), animId));
            if(vibrate) vibrator.vibrate(vibrateTime);
        }

        public void setDefaultState(){ setBackground(ContextCompat.getDrawable(context, R.drawable.buttonnormalstate)); }

        public void setFocusedState(){ setBackground(ContextCompat.getDrawable(context, R.drawable.buttonenablestate)); }

        public int getCenterX() {
            return (getLeft() + getRight()) / 2;
        }

        public int getCenterY() {
            return (getTop() + getBottom()) / 2;
        }
    }
}
