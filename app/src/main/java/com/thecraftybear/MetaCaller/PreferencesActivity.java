package com.thecraftybear.metacaller;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Switch;

import com.thecraftybear.metacaller.utils.CommonUtils;

import java.io.File;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created with IntelliJ IDEA.
 * User: Fahim
 * Date: 10/6/2016
 * Time: 12:05 AM
 */
public class PreferencesActivity extends AppCompatActivity implements View.OnClickListener{
    private ImageButton clearBtn;
    private RelativeLayout buyBtn;
    private File file;
    private ImageButton backBtn;
    private PatternManager patternManager;
    private Switch quickOpenSwitch;
    private Switch themeSwitch;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences pref = CommonUtils.getPreferences(this);
        boolean darkTheme = pref.getBoolean(getString(R.string.themeKey), false);

        if(darkTheme) setContentView(R.layout.preference_layout_dark);
        else setContentView(R.layout.preference_layout);

        clearBtn = (ImageButton) findViewById(R.id.clearDataBtn);
        buyBtn = (RelativeLayout) findViewById(R.id.buyLayout);
        backBtn = (ImageButton) findViewById(R.id.backBtn);
        quickOpenSwitch = (Switch) findViewById(R.id.quickOpenSwitch);
        themeSwitch = (Switch) findViewById(R.id.switchTheme);

        clearBtn.setOnClickListener(this);
        buyBtn.setOnClickListener(this);
        backBtn.setOnClickListener(this);
        quickOpenSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor editor = CommonUtils.getPreferencesEditor(PreferencesActivity.this);
                editor.putBoolean(getString(R.string.QuickSwitchKey), isChecked);
                editor.commit();
            }
        });

        themeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor editor = CommonUtils.getPreferencesEditor(PreferencesActivity.this);
                editor.putBoolean(getString(R.string.themeKey), isChecked);
                editor.commit();

                Intent i = new Intent();
                i.putExtra("result", true);
                setResult(10, i);
                finish();

                //showResult(false, "Please restart this app to change theme.");
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        quickOpenSwitch.setChecked(CommonUtils.getPreferences(this).getBoolean(getString(R.string.QuickSwitchKey), true));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.clearDataBtn:
                Intent i = new Intent();
                i.putExtra("result", true);
                setResult(Activity.RESULT_OK, i);
                finish();

                //showWarning("Delete all your patterns?");
                break;
            case R.id.buyLayout:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.thecraftybear.metacallerpaid")));
                break;
            case R.id.backBtn:
                finish();
                break;
        }
    }

    private void showWarning(String msg){
        final SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
        pDialog.setTitleText("Are you sure?");
        pDialog.setContentText(msg);
        pDialog.setCancelable(true);
        pDialog.setCancelText("Cancel");
        pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                pDialog.cancel();
            }
        });
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                pDialog.dismiss();
                showResult(false, "Pattern removed.");
            }
        });
        pDialog.show();
    }

    public void showResult(boolean isError, String msg){
        if(!isError) {
            final SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE);
            pDialog.setTitleText("Success");
            pDialog.setContentText(msg);
            pDialog.setCancelable(false);
            pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    pDialog.dismiss();
                }
            });
            pDialog.show();
        }else{
            final SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE);
            pDialog.setTitleText("Error");
            pDialog.setContentText(msg);
            pDialog.setCancelable(false);
            pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    pDialog.dismiss();
                }
            });
            pDialog.show();
        }
    }

    public void setPatternManager(PatternManager patternManager) {
        this.patternManager = patternManager;
    }
}
