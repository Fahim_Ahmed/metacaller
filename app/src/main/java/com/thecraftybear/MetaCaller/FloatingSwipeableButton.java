package com.thecraftybear.metacaller;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Vibrator;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;

import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.animation.ValueAnimator;
import com.thecraftybear.metacaller.utils.CommonUtils;
import com.thecraftybear.metacaller.utils.DensityUtil;

/**
 * Created by Fahim on 5/18/2016.
 */

public class FloatingSwipeableButton extends ViewGroup implements Animator.AnimatorListener {

    private int mainBtnWidth;
    private int mainBtnHeight;
    private int mainBtnSelectBackW;
    private int mainBtnSelectBackH;
    private boolean darkTheme;

    public enum OVERLAPPED_STATE{
        ENTER,
        OVERLAPPED,
        EXIT
    }

    public enum SELECTION{
        CALL,
        COPY,
        MESSAGE
    }

    private Context context;
    private boolean firstTime;

    private float cx, cy;

    private float ratio;
    private float centerX, centerY;
    private float distThreshold;
    private Paint paint;
    private SwipeView mainBtn;
    private SwipeView btnBackground;
    private SwipeView hover_call;
    private SwipeView hover_msg;
    private SwipeView hover_copy;
    private SwipeView call_view;
    private SwipeView msg_view;
    private SwipeView copy_view;
    private boolean canMove;

    private Vibrator vibrator;
    private boolean enableVibrate;
    private int vibrateTime;

    boolean onEnter = false;
    boolean onExit = false;

    Drawable CALL_ICON;
    Drawable MSG_ICON;
    Drawable COPY_ICON;

    // Communicator -------------
    private Callback callback;

    public interface Callback{
        void onRelease(SELECTION selection);
    }

    public void setCallback(Callback callback){
        this.callback = callback;
    }
    // --------------------------

    public FloatingSwipeableButton(Context context) {
        super(context);
        init(context);
    }

    public FloatingSwipeableButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public FloatingSwipeableButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

//    public FloatingSwipeableButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
//        super(context, attrs, defStyleAttr, defStyleRes);
//        init(context);
//    }

    private void init(Context context){
        this.context = context;

        darkTheme = CommonUtils.getDarkThemeResult(context);

        this.distThreshold = (float) (DensityUtil.getScreenWidth(context) * 0.25); // 25%

        float dpi = context.getResources().getDisplayMetrics().density;

        if(dpi > 3) {
            this.mainBtnWidth = 62;
            this.mainBtnHeight = 62;
            this.mainBtnSelectBackW = 72;
            this.mainBtnSelectBackH = 72;
        }else{
            this.mainBtnWidth = 48;
            this.mainBtnHeight = 48;
            this.mainBtnSelectBackW = 54;
            this.mainBtnSelectBackH = 54;
        }

        enableVibrate = true;
        vibrateTime = 20;

        if (enableVibrate && !isInEditMode()) {
            vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        }

        firstTime = true;

        setClipChildren(false);

        paint = new Paint(Paint.DITHER_FLAG);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(DensityUtil.dp2px(context, 3));
        paint.setColor(darkTheme ? Color.parseColor("#ffd54f") : Color.parseColor("#424242"));
        paint.setAntiAlias(true);

        if(darkTheme){
            CALL_ICON = ContextCompat.getDrawable(context, R.drawable.call_icon_dark);
            MSG_ICON = ContextCompat.getDrawable(context, R.drawable.msg_icon_dark);
            COPY_ICON = ContextCompat.getDrawable(context, R.drawable.copy_icon_dark);
        }else {
            CALL_ICON = ContextCompat.getDrawable(context, R.drawable.call_icon);
            MSG_ICON = ContextCompat.getDrawable(context, R.drawable.msg_icon);
            COPY_ICON = ContextCompat.getDrawable(context, R.drawable.copy_icon);
        }

        btnBackground = new SwipeView(context);
        btnBackground.setBackground(ContextCompat.getDrawable(context, (darkTheme) ? R.drawable.button_background_dark : R.drawable.button_background));
        btnBackground.setVisibility(INVISIBLE);
        addView(btnBackground);

        call_view = new SwipeView(context);
        call_view.setBackground(CALL_ICON);
        call_view.setVisibility(INVISIBLE);
        addView(call_view);

        msg_view = new SwipeView(context);
        msg_view.setBackground(MSG_ICON);
        msg_view.setVisibility(INVISIBLE);
        addView(msg_view);

        copy_view = new SwipeView(context);
        copy_view.setBackground(COPY_ICON);
        copy_view.setVisibility(INVISIBLE);
        addView(copy_view);

        hover_call = new SwipeView(context);
        hover_call.setBackground(ContextCompat.getDrawable(context, (darkTheme) ? R.drawable.hoverarea_dark : R.drawable.hoverarea));
        hover_call.setTag("call_view");
        hover_call.setAlpha(0);
        hover_msg = new SwipeView(context);
        hover_msg.setBackground(ContextCompat.getDrawable(context, (darkTheme) ? R.drawable.hoverarea_dark : R.drawable.hoverarea));
        hover_msg.setTag("msg_view");
        hover_msg.setAlpha(0);
        hover_copy = new SwipeView(context);
        hover_copy.setBackground(ContextCompat.getDrawable(context, (darkTheme) ? R.drawable.hoverarea_dark : R.drawable.hoverarea));
        hover_copy.setTag("copy_view");
        hover_copy.setAlpha(0);
        addView(hover_call);
        addView(hover_msg);
        addView(hover_copy);

        mainBtn = new SwipeView(context);
        addView(mainBtn);


        setWillNotDraw(false);
    }

    @Override
    protected int[] onCreateDrawableState(int extraSpace) {
        return super.onCreateDrawableState(extraSpace);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if(firstTime) return;

        float dx = mainBtn.getX() + mainBtn.getWidth()/2;
        float dy = mainBtn.getY() + mainBtn.getHeight()/2;

        canvas.drawLine(cx + mainBtn.getWidth()/2, cy + mainBtn.getHeight()/2, dx, dy, paint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(firstTime) {
            firstTime = false;
            cx = mainBtn.getCenterX() - mainBtn.getWidth()/2;
            cy = mainBtn.getCenterY() - mainBtn.getHeight()/2;
            Log.i("cx", cx + " " + cy);

            ratio = (getWidth() > getHeight()) ? getWidth() / getHeight() : getHeight() / getWidth();
            System.out.println("raito: " + ratio);
        }

        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                ObjectAnimator.ofFloat(mainBtn, "translationX", 0)
                        .setDuration(150)
                        .start();

                ObjectAnimator oa = ObjectAnimator.ofFloat(mainBtn, "translationY", 0);
                oa.setDuration(150);
                oa.start();

                oa.addListener(this);
                oa.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        invalidate();
                    }
                });

                if(hover_call.STATE == OVERLAPPED_STATE.OVERLAPPED) callback.onRelease(SELECTION.CALL);
                else if(hover_msg.STATE == OVERLAPPED_STATE.OVERLAPPED) callback.onRelease(SELECTION.MESSAGE);
                else if(hover_copy.STATE == OVERLAPPED_STATE.OVERLAPPED) callback.onRelease(SELECTION.COPY);

                resetSelectionState();
                canMove = false;
                hideHoverArea();
                break;
            case MotionEvent.ACTION_DOWN:
                if(isOnView(mainBtn, event.getX(), event.getY())){
                    mainBtn.setFocusedState();
                    btnBackground.setVisibility(VISIBLE);
                    canMove = true;
                    btnBackground.triggerAnim(R.anim.button_trigger, true);
                    showHoverArea();
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if(!canMove) break;

                btnBackground.setVisibility(VISIBLE);

                // touch point
                float tx = event.getX();
                float ty = event.getY();

                // view position + center offset
                centerX = (cx + mainBtn.getWidth() * 0.5f);
                centerY = (cy + mainBtn.getHeight() * 0.5f);

                // angle between view and touch
                float theta = (float) Math.atan2((centerY- ty), (tx -centerX));

                // max distance threshold
                //float dist = (float) Math.sqrt(Math.pow(Math.abs(tx - (cx + view.getWidth()*0.5)), 2) + Math.pow(Math.abs(ty - (cy + view.getHeight())), 2));
                float dist = (float) Math.sqrt(Math.pow(Math.abs(tx - centerX), 2) + Math.pow(Math.abs(ty - centerY), 2));
                if(dist >= distThreshold) dist = distThreshold;

                // new location point
                float sx = (float) (Math.cos(theta) * dist + cx);
                float sy = (float) (Math.sin(theta) * dist * -1 + cy);

                isOverlapped(hover_call);
                isOverlapped(hover_msg);
                isOverlapped(hover_copy);

                //System.out.println("-----> " + tx + " " + ty);

                mainBtn.animate()
                        .x(sx)
                        .y(sy)
                        .setDuration(0)
                        .start();

                invalidate();
                break;
        }

        return true;
    }

    private void resetSelectionState() {
        hover_call.STATE = OVERLAPPED_STATE.EXIT;
        hover_msg.STATE = OVERLAPPED_STATE.EXIT;
        hover_copy.STATE = OVERLAPPED_STATE.EXIT;
    }

    private void showHoverArea() {
        ObjectAnimator.ofFloat(hover_call, "alpha", 1)
                .setDuration(250)
                .start();

        ObjectAnimator.ofFloat(hover_msg, "alpha", 1)
                .setDuration(250)
                .start();

        ObjectAnimator.ofFloat(hover_copy, "alpha", 1)
                .setDuration(250)
                .start();
    }

    private void hideHoverArea() {
        ObjectAnimator.ofFloat(hover_call, "alpha", 0)
                .setDuration(250)
                .start();

        ObjectAnimator.ofFloat(hover_msg, "alpha", 0)
                .setDuration(250)
                .start();

        ObjectAnimator.ofFloat(hover_copy, "alpha", 0)
                .setDuration(250)
                .start();
    }

    private boolean isOnView(View view, float x, float y) {
        return (x >= view.getLeft() && x <= view.getRight() &&
                y >= view.getTop() && y <= view.getBottom());
    }

    private boolean isOverlapped(SwipeView targetView){
        float dx = mainBtn.getX() + mainBtn.getWidth()/2;
        float dy = mainBtn.getY() + mainBtn.getHeight()/2;

        float tx = targetView.getCenterX();
        float ty = targetView.getCenterY();

        int dist = (int) Math.sqrt(Math.pow(Math.abs(tx-dx), 2) +  Math.pow(Math.abs(ty-dy), 2));

        if(dist < mainBtn.getWidth()/2){
            if(targetView.STATE == OVERLAPPED_STATE.ENTER) {
                targetView.STATE = OVERLAPPED_STATE.OVERLAPPED;

                vibrator.vibrate(vibrateTime);

                if(targetView.getTag() == "call_view"){
                    call_view.setVisibility(VISIBLE);
                }else if(targetView.getTag() == "msg_view"){
                    msg_view.setVisibility(VISIBLE);
                }else if(targetView.getTag() == "copy_view"){
                    copy_view.setVisibility(VISIBLE);
                }
            }

            if(targetView.STATE == OVERLAPPED_STATE.EXIT)
                targetView.STATE = OVERLAPPED_STATE.ENTER;

            return true;
        }else {
            targetView.STATE = OVERLAPPED_STATE.EXIT;

            if(targetView.getTag() == "call_view"){
                call_view.setVisibility(INVISIBLE);
            }else if(targetView.getTag() == "msg_view"){
                msg_view.setVisibility(INVISIBLE);
            }else if(targetView.getTag() == "copy_view"){
                copy_view.setVisibility(INVISIBLE);
            }
        }

        return false;
    }

    public void showCallState(){
        mainBtn.isCallState = true;
        mainBtn.setBackground(ContextCompat.getDrawable(context, darkTheme ? R.drawable.buttoncallstate_dark : R.drawable.buttoncallstate));
    }

    public void showNormalState(){
        mainBtn.isCallState = false;
        mainBtn.setBackground(ContextCompat.getDrawable(context, darkTheme ? R.drawable.buttonnormalstate_dark : R.drawable.buttonnormalstate));
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Drawable d = btnBackground.getBackground();

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int dw = d.getIntrinsicWidth();
        int dh = d.getIntrinsicHeight();

        if(dw == 0) dw = DensityUtil.dp2px(context, 20);
        if(dh == 0) dw = DensityUtil.dp2px(context, 20);

        //Measure Width
        int width, height;

        if (widthMode == MeasureSpec.EXACTLY) {
            //Must be this size
            width = widthSize;
        } else if (widthMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            width = Math.min(dw, widthSize);
        } else {
            //Be whatever you want
            width = dw;
        }

        //Measure Height
        if (heightMode == MeasureSpec.EXACTLY) {
            //Must be this size
            height = heightSize;
        } else if (heightMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            height = Math.min(dh, heightSize);
        } else {
            //Be whatever you want
            height = dh;
        }

        setMeasuredDimension(width, height);
    }


    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        if(changed){
            int offsetBotY = DensityUtil.dp2px(context, 16);

            int areaWidth = r-l;
            int areaHeight = b-t;

            int bigBtnW = DensityUtil.dp2px(context, mainBtnWidth);
            int bigBtnH = DensityUtil.dp2px(context, mainBtnHeight);

            int ml = (areaWidth - bigBtnW) / 2;
            int mr = ml + bigBtnW;
            int mt = (3 * areaHeight / 4) - bigBtnH - offsetBotY; //(areaHeight - btnH) / 2;
            int mb = mt + bigBtnH;
            mainBtn.layout(ml, mt, mr, mb);

            bigBtnW = DensityUtil.dp2px(context, mainBtnSelectBackW);
            int bigBtnBackH = DensityUtil.dp2px(context, mainBtnSelectBackH);

            ml = (areaWidth - bigBtnW) / 2;
            mr = ml + bigBtnW;
            mt = (3 * areaHeight / 4) - (bigBtnBackH + bigBtnH)/2 - offsetBotY; //(areaHeight - btnH) / 2;
            mb = mt + bigBtnBackH;
            btnBackground.layout(ml, mt, mr, mb);

            int btnW = DensityUtil.dp2px(context, 24);
            int btnH = DensityUtil.dp2px(context, 24);

            ml = (areaWidth - btnW) / 2;
            mr = ml + btnW;
            mt = (3 * areaHeight / 4) - (bigBtnH + btnH)/2 - offsetBotY; //(areaHeight - btnH) / 2;
            mb = mt + btnH;
            call_view.layout(ml, mt, mr, mb);
            msg_view.layout(ml, mt, mr, mb);
            copy_view.layout(ml, mt, mr, mb);

            btnW = DensityUtil.dp2px(context, 12);
            btnH = DensityUtil.dp2px(context, 12);

            ml = 3*areaWidth/10 - btnW;
            mr = ml + btnW;
            mt = (3 * areaHeight / 4) - (bigBtnH + btnH)/2 - offsetBotY; // (areaHeight - btnH) / 2;
            mb = mt + btnH;
            hover_msg.layout(ml, mt, mr, mb);

            ml = 7*areaWidth/10 + btnW/2;
            mr = ml + btnW;
            mt = (3 * areaHeight / 4) - (bigBtnH + btnH)/2 - offsetBotY; // (areaHeight - btnH) / 2;
            mb = mt + btnH;
            hover_call.layout(ml, mt, mr, mb);

            ml = (areaWidth - btnW) / 2;
            mr = ml + btnW;
            mt = (3 * areaHeight / 4) - (bigBtnH + btnH)/2 - offsetBotY - (int)distThreshold;
            mb = mt + btnH;
            hover_copy.layout(ml, mt, mr, mb);
        }
    }

    @Override
    public void onAnimationStart(Animator animation) {

    }

    @Override
    public void onAnimationEnd(Animator animation) {
        btnBackground.setVisibility(INVISIBLE);
        mainBtn.setDefaultState();
    }

    @Override
    public void onAnimationCancel(Animator animation) {

    }

    @Override
    public void onAnimationRepeat(Animator animation) {

    }

    class SwipeView extends View {
        public OVERLAPPED_STATE STATE = OVERLAPPED_STATE.EXIT;
        public boolean isCallState;

        public SwipeView(Context context) {
            super(context);
            setBackground(ContextCompat.getDrawable(context, (darkTheme) ? R.drawable.buttonnormalstate_dark : R.drawable.buttonnormalstate));
            setClipChildren(false);
        }

        public SwipeView(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public SwipeView(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
        }

        public void triggerAnim(int animId, boolean vibrate){
            startAnimation(AnimationUtils.loadAnimation(getContext(), animId));
            if(vibrate) vibrator.vibrate(vibrateTime);
        }

        public void setDefaultState(){
            if(darkTheme) setBackground(ContextCompat.getDrawable(context, isCallState ? R.drawable.buttoncallstate_dark : R.drawable.buttonnormalstate_dark));
            else setBackground(ContextCompat.getDrawable(context, isCallState ? R.drawable.buttoncallstate : R.drawable.buttonnormalstate));
        }

        public void setFocusedState(){
            if(darkTheme) setBackground(ContextCompat.getDrawable(context, (isCallState) ? R.drawable.buttoncallstatepressed_dark : R.drawable.buttonenablestate_dark));
            else setBackground(ContextCompat.getDrawable(context, (isCallState) ? R.drawable.buttoncallstatepressed : R.drawable.buttonenablestate));

        }

        public int getCenterX() {
            return (getLeft() + getRight()) / 2;
        }

        public int getCenterY() {
            return (getTop() + getBottom()) / 2;
        }

        public  int getBtnWidth(){ return getRight() - getLeft(); }

        public  int getBtnHeight(){ return getTop() - getBottom(); }
    }
}
