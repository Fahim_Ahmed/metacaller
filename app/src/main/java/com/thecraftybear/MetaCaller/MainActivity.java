package com.thecraftybear.metacaller;

import android.Manifest;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.gordonwong.materialsheetfab.MaterialSheetFab;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import com.thecraftybear.metacaller.adapters.BottomSheetListViewAdapter;
import com.thecraftybear.metacaller.adapters.ContactListViewAdapter;
import com.thecraftybear.metacaller.data.PatternNode;
import com.thecraftybear.metacaller.dialogs.ContactListDialog;
import com.thecraftybear.metacaller.dialogs.PatternRecordDialog;
import com.thecraftybear.metacaller.interfaces.Communicator;
import com.thecraftybear.metacaller.utils.CommonUtils;
import com.thecraftybear.metacaller.utils.DensityUtil;

import net.alhazmy13.gota.Gota;
import net.alhazmy13.gota.GotaResponse;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, Communicator, Gota.OnRequestPermissionsBack, OnShowcaseEventListener {

    private TextView lightNumberView;
    private PatternFragment patternFragment;
    private FragmentManager fManager;
    private PatternRecordDialog patternRecordDialog;
    private PatternManager patternManager;
    private ContactManager contactManager;
    private SwipeMenuListView bottomListView;
    private SwipeMenuListView listViewSidebar;
    private SlidingUpPanelLayout mainLayout;
    public boolean isBottomViewOpen;
    private ContactListViewAdapter contactListController;
    private BottomSheetListViewAdapter bottomSheetListController;
    private DrawerLayout rootLayout;
    private NavigationView drawerView;
    private EditText searchView;
    private ImageButton clearBtnView;
    private TextView patternEmptyView;
    private TextView linkedContactCountView;
    private EditText phoneNumkView;
    private TextView nameView;
    private String currentPattern;
    private FloatingSwipeableButton FSB;
    private MaterialSheetFab materialSheetFab;
    private SweetAlertDialog infoDialog;
    private ShowcaseView sv;
    private int tutCounter = 0;
    private boolean firstRun = false;
    private boolean openMenu = false;
    private ContactListDialog contactsDialog;
    public static boolean DARK_THEME = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences pref = CommonUtils.getPreferences(this);
        DARK_THEME = pref.getBoolean(getString(R.string.themeKey), false);

        float scale = getResources().getDisplayMetrics().density;

        if(DARK_THEME) setContentView((scale > 3) ? R.layout.main_activity_layout_xxxhdpi_dark : R.layout.main_activity_layout_dark);
        else setContentView((scale > 3) ? R.layout.main_activity_layout_xxxhdpi : R.layout.main_activity_layout);

        currentPattern = "";

        patternFragment = new PatternFragment();
        fManager = getSupportFragmentManager();
        FragmentTransaction transaction = fManager.beginTransaction();
        transaction.add(R.id.patternFragmentView, patternFragment, "patternFragment");
        transaction.commit();

        initSmallViews();
        requestPermissions();
    }

    private void initSmallViews() {
        Button sidebarBtn = (Button) findViewById(R.id.sidebarBtn);
        ImageButton logBtn = (ImageButton) findViewById(R.id.logBtn);
        ImageButton addBtn = (ImageButton) findViewById(R.id.addBtn);

        searchView = (EditText) findViewById(R.id.searchTextView);
        clearBtnView = (ImageButton) findViewById(R.id.searchClearBtn);

        Button contactBtn = (Button) findViewById(R.id.contactBtn);
        Button prefBtn = (Button) findViewById(R.id.prefBtn);

        sidebarBtn.setOnClickListener(this);
        logBtn.setOnClickListener(this);
        addBtn.setOnClickListener(this);
        contactBtn.setOnClickListener(this);
        prefBtn.setOnClickListener(this);
        clearBtnView.setOnClickListener(this);
    }

    private void init() {
        initPhoneNumberInputView();
        initDataClasses();
        initBottomSheet();
        initSidebar();
        initFAB();
        initFSB();

        TextView emailNameView = (TextView) findViewById(R.id.emailNameView);
        TextView emailView = (TextView) findViewById(R.id.emailView);

        String email = CommonUtils.getEmail(this);
        if (email != null) {
            String[] tokens = email.split("@");

            if (emailNameView != null) emailNameView.setText(tokens[0]);
            if (emailView != null) emailView.setText(tokens[1]);
        }

        infoDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        infoDialog.getProgressHelper().setBarColor(Color.parseColor("#ffd54f"));
        infoDialog.setTitleText("Initializing...");
        infoDialog.setCancelable(true);

        SharedPreferences pref = CommonUtils.getPreferences(this);
        firstRun = pref.getBoolean(getResources().getString(R.string.firstrun), true);
        openMenu = pref.getBoolean(getString(R.string.QuickSwitchKey), true);

        if(firstRun) {
            sv = new ShowcaseView.Builder(this)
                    //.withMaterialShowcase()
                    .setContentTitle(getString(R.string.tut_init))
                    .setContentText(getString(R.string.tut_init_description))
//                        "Also, the main purpose of this app is to do basic tasks quickly like call, copy or share a number. " +
//                        "You set a pattern for a number and draw that pattern to call, copy or share. " +
//                        "This app is basically like a speed dialer but with patterns instead of numbers."
                    .setStyle(R.style.CustomShowcaseTheme)
                    .setShowcaseEventListener(this)
                    .replaceEndButton(R.layout.tutorial_button_layout)
                    //.hideOnTouchOutside()
                    .build();
        }
    }

    private void copyText(String text) {
        String newText;

        PatternNode node = contactManager.hasThisNode(text);
        if (node == null) newText = text;
        else {
            newText = node.getContactName();
            newText += "\n";
            newText += node.getContactNumber();
        }

        ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("contact", newText);
        clipboard.setPrimaryClip(clip);

        Toast.makeText(this, "Copied to clipboard.", Toast.LENGTH_SHORT).show();
    }

    private void showRunningDialog(String msg) {
        infoDialog.setTitleText(msg);
        infoDialog.show();
    }

    private void requestPermissions() {
        new Gota.Builder(this)
                .withPermissions(Manifest.permission.GET_ACCOUNTS, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .setListener(this)
                .check();
    }

    @Override
    public void onRequestBack(GotaResponse gotaResponse) {
        if (gotaResponse.isGranted(Manifest.permission.GET_ACCOUNTS) && gotaResponse.isGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            init();
        } else {
            showResult(true, getString(R.string.permission_denied));
        }
    }

    private void initDataClasses() {
        patternEmptyView = (TextView) findViewById(R.id.emptyView);
        Button patternButtonView = (Button) findViewById(R.id.patternAddBtn);
        linkedContactCountView = (TextView) findViewById(R.id.contactNumTextView);

        patternManager = new PatternManager(this);
        bottomSheetListController = new BottomSheetListViewAdapter(this, patternManager.readPatternData(), linkedContactCountView, patternEmptyView, patternButtonView, patternManager);
        patternManager.setPatternListController(bottomSheetListController);

        contactManager = new ContactManager(this, patternManager);
        contactListController = new ContactListViewAdapter(this, contactManager.getContacts(), searchView, clearBtnView);
        contactManager.setListController(contactListController);

        patternRecordDialog = new PatternRecordDialog();
        patternRecordDialog.setManager(patternManager);

        patternFragment.setManager(patternManager);

        if (patternButtonView != null) patternButtonView.setOnClickListener(this);
    }

    private void initBottomSheet() {
        mainLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        FrameLayout bottomSheetLayout = (FrameLayout) findViewById(R.id.bottom_sheet);

        int count = patternManager.getSize();
        linkedContactCountView.setText((count == 1) ? "1 CONTACT" : count + " CONTACTS");
        if (!patternManager.isEmpty()) patternEmptyView.setVisibility(View.INVISIBLE);

        int currentHeight = DensityUtil.getScreenHeight(this);

        float scale = getResources().getDisplayMetrics().density;
        bottomSheetLayout.getLayoutParams().height = currentHeight - DensityUtil.dp2px(this, (scale > 3) ? 175 : 160);
        bottomSheetLayout.requestLayout();

        mainLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {

            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                if (newState == SlidingUpPanelLayout.PanelState.EXPANDED) {
                    if(materialSheetFab.isSheetVisible()) materialSheetFab.hideSheet();

                    patternFragment.setTouchable(false);
                    isBottomViewOpen = true;

                    if(openMenu) openBottomSwipeMenu();

                    if(firstRun && bottomSheetListController.getCount() > 0) {
                        if(!openMenu) openBottomSwipeMenu();

                        sv = new ShowcaseView.Builder(MainActivity.this)
                                .setTarget(new ViewTarget(R.id.dummy2, MainActivity.this))
                                .setContentTitle(getString(R.string.tut_contact_title))
                                .setContentText(getString(R.string.tut_contact_description))
                                .setStyle(R.style.CustomShowcaseTheme)
                                .setShowcaseEventListener(MainActivity.this)
                                .replaceEndButton(R.layout.tutorial_button_layout)
                                .withMaterialShowcase()
                                .build();

                        tutCounter++;
                        firstRun = false;
                        SharedPreferences.Editor editor = CommonUtils.getPreferencesEditor(MainActivity.this);
                        editor.putBoolean(getResources().getString(R.string.firstrun), false);
                        editor.commit();
                    }
                } else if (newState == SlidingUpPanelLayout.PanelState.COLLAPSED) {
                    patternFragment.setTouchable(true);
                    isBottomViewOpen = false;
                } else if (newState == SlidingUpPanelLayout.PanelState.HIDDEN) {
                    mainLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                }
            }
        });

        bottomListView = (SwipeMenuListView) mainLayout.findViewById(R.id.contactListView);
        bottomListView.setAdapter(bottomSheetListController);
        patternManager.setListView(bottomListView);

        SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                // create "open" item
                SwipeMenuItem openItem = new SwipeMenuItem(getApplicationContext());
                openItem.setBackground(R.drawable.yellow_button_selection);
                openItem.setWidth(DensityUtil.dp2px(MainActivity.this, 64));
                openItem.setTitleSize(12);
                openItem.setIcon(R.mipmap.pttrnico);
                openItem.setTitleColor(ContextCompat.getColor(MainActivity.this, R.color.dark_gray_26));
                menu.addMenuItem(openItem);

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(getApplicationContext());
                deleteItem.setBackground(DARK_THEME ? R.drawable.dark_selection_20 : R.drawable.dark_button_selection);
                deleteItem.setWidth(DensityUtil.dp2px(MainActivity.this, 60));
                deleteItem.setIcon(R.mipmap.trash);
                menu.addMenuItem(deleteItem);
            }
        };

        bottomListView.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);
        bottomListView.setMenuCreator(creator);
        bottomListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                //bottomSheetListController.editedIndex = position;
                switch (index) {
                    case 0:
                        showPatternAddDialog(bottomSheetListController.getItem(position));
                        break;
                    case 1:
                        showWarning("Remove this pattern?", position);
                        break;
                }
                return false;
            }
        });

        bottomListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                updatePhoneNumberView(bottomListView, position);
                mainLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }
        });
    }

    private void openSidebarSwipeMenu(){
        if(contactListController.getCount() > 0) {
            listViewSidebar.smoothOpenMenu(0);
        }
    }

    private void openBottomSwipeMenu(){
        if(bottomListView.getCount() > 0)
            bottomListView.smoothOpenMenu(0);
    }

    private void initFSB() {
        FSB = (FloatingSwipeableButton) findViewById(R.id.swipeBtn);
        FSB.setCallback(new FloatingSwipeableButton.Callback() {
            @Override
            public void onRelease(FloatingSwipeableButton.SELECTION selection) {
                //System.out.println(" --> " + selection.toString());

                if (phoneNumkView.getText().toString().contains("HELLO"))
                    return;
                if (phoneNumkView.getText().toString().trim().length() == 0)
                    return;

                switch (selection) {
                    case CALL:
                        new Gota.Builder(MainActivity.this)
                                .withPermissions(Manifest.permission.CALL_PHONE)
                                .setListener(new Gota.OnRequestPermissionsBack() {
                                    @Override
                                    public void onRequestBack(GotaResponse gotaResponse) {
                                        if (gotaResponse.isGranted(Manifest.permission.CALL_PHONE)) {
                                            String phoenNum = phoneNumkView.getText().toString();
                                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                                            if(phoenNum.contains("#"))
                                                callIntent.setData(ussdToCallableUri(phoenNum));
                                            else {
                                                showRunningDialog("Calling...");
                                                callIntent.setData(Uri.parse("tel:" + phoenNum));
                                            }
                                            startActivity(callIntent);
                                        }else{
                                            showResult(true, "Permission was denied. Consider granting it in order to function this app properly.");
                                        }
                                    }
                                })
                                .check();
                        break;
                    case MESSAGE:
                        showRunningDialog("Launching SMS intent...");
                        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                        sendIntent.setData(Uri.parse("sms:" + phoneNumkView.getText().toString()));
                        startActivity(sendIntent);
                        break;
                    case COPY:
                        copyText(phoneNumkView.getText().toString());
                        break;
                }
            }
        });
    }

    private void initSidebar() {
        rootLayout = (DrawerLayout) findViewById(R.id.mainLayout);
        drawerView = (NavigationView) findViewById(R.id.nav_view);

        rootLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                if(openMenu) openSidebarSwipeMenu();

                if(firstRun && tutCounter < 5) {
                    if(!openMenu) openSidebarSwipeMenu();

                    sv = new ShowcaseView.Builder(MainActivity.this)
                            .setTarget(new ViewTarget(R.id.dummy, MainActivity.this))
                            .setContentTitle(getString(R.string.tut_sidebar_title))
                            .setContentText(getString(R.string.tut_sidebar_description))
                            .setStyle(R.style.CustomShowcaseTheme)
                            .setShowcaseEventListener(MainActivity.this)
                            .replaceEndButton(R.layout.tutorial_button_layout)
                            .withNewStyleShowcase()
                            .build();
                    tutCounter++;
                }
            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });

        listViewSidebar = (SwipeMenuListView) findViewById(R.id.sidebarListView);
        if (listViewSidebar != null) listViewSidebar.setAdapter(contactListController);
        contactManager.setListView(listViewSidebar);

        SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {

                switch (menu.getViewType()){
                    case 0:
                        // add pattern
                        SwipeMenuItem openItem = new SwipeMenuItem(getApplicationContext());
                        openItem.setBackground(R.drawable.yellow_button_selection);
                        openItem.setWidth(DensityUtil.dp2px(MainActivity.this, 64));
                        openItem.setTitleSize(12);
                        openItem.setIcon(R.mipmap.pttrnico);
                        openItem.setTitleColor(ContextCompat.getColor(MainActivity.this, R.color.dark_gray_26));
                        menu.addMenuItem(openItem);
                        break;
                    case 1:
                        SwipeMenuItem openItem_edit = new SwipeMenuItem(getApplicationContext());
                        openItem_edit.setBackground(DARK_THEME ? R.drawable.dark_selection_20 : R.drawable.dark_button_selection);
                        openItem_edit.setWidth(DensityUtil.dp2px(MainActivity.this, 64));
                        openItem_edit.setTitleSize(12);
                        openItem_edit.setIcon(R.mipmap.edit_icon);
                        menu.addMenuItem(openItem_edit);
                        break;
                }
            }
        };

        listViewSidebar.setSwipeDirection(SwipeMenuListView.DIRECTION_RIGHT);
        listViewSidebar.setMenuCreator(creator);
        listViewSidebar.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        PatternNode n = contactListController.getItem(position);
                        showPatternAddDialog(n);
                        break;
                }
                return false;
            }
        });

        listViewSidebar.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                updatePhoneNumberView(listViewSidebar, position);
                rootLayout.closeDrawer(drawerView);
            }
        });

        contactsDialog = new ContactListDialog();
        contactsDialog.setExtraVars(this, contactManager, patternManager);
    }

    private void initFAB() {
        Fab fab = (Fab) findViewById(R.id.fab);
        View sheetView = findViewById(R.id.fab_sheet);
        View overlay = findViewById(R.id.overlay);
        int sheetColor = ContextCompat.getColor(this, R.color.white_95);
        int fabColor = ContextCompat.getColor(this, R.color.yellow);

        System.out.println(" --> " + (fab == null) + " " + (sheetView == null));
        // Initialize material sheet FAB
        materialSheetFab = new MaterialSheetFab<>(fab, sheetView, overlay, sheetColor, fabColor);
        //materialSheetFab.setEventListener();

        TextView tv_share = (TextView) findViewById(R.id.fab_sheet_item_share);
        TextView tv_profile = (TextView) findViewById(R.id.fab_sheet_item_profile);
        TextView tv_addcon = (TextView) findViewById(R.id.fab_sheet_item_add_contact);
        TextView tv_pref = (TextView) findViewById(R.id.fab_sheet_item_pref);

        if (tv_share != null) tv_share.setOnClickListener(this);
        if (tv_profile != null) tv_profile.setOnClickListener(this);
        if (tv_addcon != null) tv_addcon.setOnClickListener(this);
        if (tv_pref != null) tv_pref.setOnClickListener(this);

        //if(android.os.Build.MANUFACTURER.equalsIgnoreCase("samsung")) tv_profile.setVisibility(View.GONE);
    }

    private Uri ussdToCallableUri(String ussd) {
        String uriString = "";

        if(!ussd.startsWith("tel:"))
            uriString += "tel:";

        for(char c : ussd.toCharArray()) {
            if(c == '#') uriString += Uri.encode("#");
            else uriString += c;
        }

        return Uri.parse(uriString);
    }

    private void updatePhoneNumberView(ListView listView, int position) {
        if(listView.getAdapter().getCount() == 0) return;

        PatternNode node = (PatternNode) listView.getAdapter().getItem(position);
        phoneNumkView.setText((node == null) ? getResources().getString(R.string.helloworld) : node.getContactNumber());
        nameView.setText((node == null) ? getResources().getString(R.string.dummy_name) : node.getContactName().toUpperCase());
    }

    public void showWarning(String msg, final int index){
        final SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
        pDialog.setTitleText("Are you sure?");
        pDialog.setContentText(msg);
        pDialog.setCancelable(true);
        pDialog.setCancelText("Cancel");
        pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                pDialog.cancel();
            }
        });
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                String code = bottomSheetListController.getItem(index).getPatternCode(); //patternManager.getNode(index).getPatternCode();
                contactManager.removeCode(code);
                patternManager.removeNode(code);

                pDialog.dismiss();
                showResult(false, "Pattern removed.");
            }
        });
        pDialog.show();
    }

    private void showWarningForClean(String msg){
        final SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
        pDialog.setTitleText("Are you sure?");
        pDialog.setContentText(msg);
        pDialog.setCancelable(true);
        pDialog.setCancelText("Cancel");
        pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                pDialog.cancel();
            }
        });
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                patternManager.cleanData();
                contactManager.cleanData();
                pDialog.dismiss();
                showResult(false, "Data removed.");
            }
        });
        pDialog.show();
    }

    public void showResult(boolean isError, String msg){
        if(!isError) {
            final SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE);
            pDialog.setTitleText("Success");
            pDialog.setContentText(msg);
            pDialog.setCancelable(false);
            pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    pDialog.dismissWithAnimation();
                }
            });
            pDialog.show();
        }else{
            final SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE);
            pDialog.setTitleText("Error");
            pDialog.setContentText(msg);
            pDialog.setCancelable(false);
            pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    pDialog.dismissWithAnimation();
                }
            });
            pDialog.show();
        }
    }

    @Override
    public void onClick(View v) {
        String text;

        switch (v.getId()){
            case R.id.addBtn:
                String str = phoneNumkView.getText().toString();

                if(currentPattern.length() > 1){
                    PatternNode node = patternManager.getNode(currentPattern);
                    if(node != null) showResult(true, "Duplicate Pattern.");
                    else contactsDialog.show(fManager, "contactsDialog");

                }else if(str.contains("HELLO") || str.isEmpty()){
                    showResult(true, "Invalid number.");
                    break;
                }else {
                    PatternNode node = contactManager.hasThisNode(str);
                    if (node == null) {
                        node = new PatternNode();
                        node.setContactNumber(str);
                    }
                    showPatternAddDialog(node);
                }
                break;
            case R.id.sidebarBtn:
                rootLayout.openDrawer(drawerView);
                break;
            case R.id.contactBtn:
                Intent showContacts = new Intent();
                showContacts.setAction(Intent.ACTION_VIEW);
                showContacts.setType(ContactsContract.Contacts.CONTENT_TYPE);
                startActivity(showContacts);
                break;
            case R.id.logBtn:
                Intent showCallLog = new Intent();
                showCallLog.setAction(Intent.ACTION_VIEW);
                showCallLog.setType(CallLog.Calls.CONTENT_TYPE);
                startActivity(showCallLog);
                break;
            case R.id.searchClearBtn:
                if(searchView.getText().length() > 0)
                    searchView.setText("");
                else{
                    searchView.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(searchView, InputMethodManager.SHOW_IMPLICIT);
                }
                break;
            case R.id.fab_sheet_item_share:
                text = phoneNumkView.getText().toString();
                if(text.contains("HELLO")){
                    showResult(true, "Invalid Number");
                    break;
                }

                PatternNode _node = contactManager.hasThisNode(text);
                String newText;

                if(_node == null) newText = text;
                else{
                    newText = _node.getContactName();
                    newText += "\n";
                    newText += _node.getContactNumber();
                }

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, newText);
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, "Send to"));
                break;
            case R.id.fab_sheet_item_profile:
                PatternNode n = contactManager.hasThisNode(phoneNumkView.getText().toString());
                if(n == null) showResult(true, "Profile not found.");
                else {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI, n.get_id());
                    intent.setData(uri);
                    startActivity(intent);
                }
                break;
            case R.id.fab_sheet_item_add_contact:
                text = phoneNumkView.getText().toString();
                if(text.contains("HELLO")){
                    showResult(true, "Invalid Number");
                    break;
                }

                Intent intent = new Intent(Intent.ACTION_INSERT);
                intent.setType(ContactsContract.Contacts.CONTENT_TYPE);

                intent.putExtra(ContactsContract.Intents.Insert.PHONE, text);
                startActivity(intent);
                break;
            case R.id.prefBtn:
            case R.id.fab_sheet_item_pref:
                Intent i = new Intent(MainActivity.this, PreferencesActivity.class);
                startActivityForResult(i, 2);
                break;
            case R.id.patternAddBtn:
                rootLayout.openDrawer(drawerView);
                openMenu = true;
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 2){
            if(materialSheetFab.isSheetVisible()) materialSheetFab.hideSheet();
            SharedPreferences preferences = CommonUtils.getPreferences(this);
            openMenu = preferences.getBoolean(getString(R.string.QuickSwitchKey), true);

            if(resultCode == Activity.RESULT_OK){
                boolean i = data.getBooleanExtra("result", false);
                if(i) showWarningForClean("Delete all your patterns?");
            }

            if(resultCode == 10){
                startActivity(new Intent(MainActivity.this, BlankActivity.class));
                finish();
            }
        }
    }

    @Override
    public void onRespond(String oldCode, String newCode){
        PatternNode node = contactManager.findByCode(oldCode);
        if(node != null) node.setPatternCode(newCode);
        contactListController.notifyDataSetChanged();
        listViewSidebar.invalidate();
    }

    @Override
    public void onSuccess() {
        contactListController.notifyDataSetChanged();
    }

    @Override
    public void updateMainNumberView(int position) {
        updatePhoneNumberView(bottomListView, position);
        mainLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
    }

    @Override
    public void onDrawPattern(String password) {
        if(password.equalsIgnoreCase("-1") || password.length() < 2){
            resetPhoneNumber();
            password = "";
        }else FSB.showCallState();

        currentPattern = password;
        bottomSheetListController.onPatternDraw(password);
    }

    @Override
    public void onChangeFilter(int position) {
        if(currentPattern == null) return;

        if(currentPattern.length() == 0) {
            resetPhoneNumber();
            FSB.showNormalState();
            return;
        }
        updatePhoneNumberView(bottomListView, position);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(infoDialog != null) infoDialog.dismiss();
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println(" --> " + "RESUME");
        if(bottomListView != null) contactManager.refreshContacts(contactListController);
        //if(infoDialog != null) infoDialog.dismiss();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(contactManager != null) contactManager.cacheContacts();
    }

    @Override
    public void onBackPressed() {
        if(isBottomViewOpen){
            //bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            mainLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        }else if (materialSheetFab.isSheetVisible()) {
            materialSheetFab.hideSheet();
        } else {
            //contactManager.cacheContacts();
            super.onBackPressed();
        }
    }

    private void initPhoneNumberInputView() {
        phoneNumkView = (EditText) findViewById(R.id.mainNumberView);
        if (phoneNumkView != null){
            phoneNumkView.setSelected(false);
            phoneNumkView.setCursorVisible(false);
        }
        phoneNumkView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                phoneNumkView.setCursorVisible(true);
                if(phoneNumkView.getText().toString().contains("H")) phoneNumkView.selectAll();
                if(mainLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED)
                     mainLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }
        });

        setupHideIME(findViewById(R.id.mainLayout));
        //requestPermissions("android.permission.READ_SMS", );

        phoneNumkView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                phoneNumkView.setCursorVisible(false);

                if(phoneNumkView.getText().length() == 0){
                    resetPhoneNumber();
                    if(FSB != null) FSB.showNormalState();
                }

                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(phoneNumkView.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }

                return false;
            }
        });

        phoneNumkView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                lightNumberView.setText(s);
                if(FSB != null) FSB.showCallState();

                if(patternManager != null && !patternFragment.isDrawing) {
                    PatternNode node = patternManager.containThisNumber(s.toString());
                    if (node != null) {
                        patternFragment.setPattern(node.getPatternCode());
                        currentPattern = node.getPatternCode();
                    }else{
                        patternFragment.setPattern("");
                        currentPattern = "";
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(phoneNumkView.isCursorVisible()) return;

                phoneNumkView.startAnimation(AnimationUtils.loadAnimation(MainActivity.this, R.anim.fadein));
                //lightNumberView.startAnimation(AnimationUtils.loadAnimation(MainActivity.this, android.R.anim.fade_in));
            }
        });

        lightNumberView = (TextView) findViewById(R.id.lightNumView);
        setTypeFace(lightNumberView);

        phoneNumkView.setText(R.string.helloworld);
        lightNumberView.setText(R.string.helloworld);
        nameView = (TextView) findViewById(R.id.nameView);
    }

    private void resetPhoneNumber(){
        phoneNumkView.setText(R.string.helloworld);
        nameView.setText(R.string.dummy_name);
    }

    private void setupHideIME(View view) {
        //Set up touch listener for non-text box views to hide keyboard.
        if(!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(MainActivity.this);
                    phoneNumkView.setCursorVisible(false);

                    if(phoneNumkView.getText().length() == 0){
                        phoneNumkView.setText(R.string.helloworld);
                        nameView.setText(R.string.dummy_name);
                    }

                    if (phoneNumkView.hasSelection()) phoneNumkView.setSelection(0,0);

                    return false;
                }
            });
        }
        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            int len = ((ViewGroup) view).getChildCount();
            for (int i = 0; i < len; i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupHideIME(innerView);
            }
        }
    }

    public void showPatternAddDialog(PatternNode selectedContact){
        patternRecordDialog.show(selectedContact, fManager, "patternRecordDialog");
    }

    private void setTypeFace(TextView view){
        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/consolab.ttf");
        view.setTypeface(type);
    }

    private void hideSoftKeyboard(Activity activity){
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public void onShowcaseViewHide(ShowcaseView showcaseView) {

    }

    @Override
    public void onShowcaseViewDidHide(ShowcaseView showcaseView) {
        switch (tutCounter){
            case 0:
                sv.setTarget(new ViewTarget(R.id.swipeBtn, this));
                sv.setContentTitle(getString(R.string.tut_mainbtn_title));
                sv.setContentText(getString(R.string.tut_mainbtn_description));
                sv.show();
                tutCounter++;
                break;
            case 1:
                sv = new ShowcaseView.Builder(this)
                        .withMaterialShowcase()
                        .setTarget(new ViewTarget(R.id.addBtn, this))
                        .setContentTitle(getString(R.string.tut_addpttrn_title))
                        .setContentText(getString(R.string.tut_addpttrn_description))
                        .setStyle(R.style.CustomShowcaseTheme)
                        .setShowcaseEventListener(this)
                        .replaceEndButton(R.layout.tutorial_button_layout)
                        .build();

                tutCounter++;
                break;
            case 2:
                sv.setTarget(new ViewTarget(R.id.logBtn, this));
                sv.setContentTitle(getString(R.string.tut_callog_title));
                sv.setContentText(getString(R.string.tut_calllog_description));
                sv.show();
                tutCounter++;
                break;
            case 3:
                sv.setTarget(new ViewTarget(R.id.bottom_ticker, this));
                sv.setContentTitle(getString(R.string.tut_botton_title));
                sv.setContentText(getString(R.string.tut_bottom_description));
                sv.show();
                tutCounter++;
                break;
//            case 4:
//                sv.setTarget(new ViewTarget(R.id.sidebarBtn, this));
//                sv.setContentTitle("Sidebar");
//                sv.setContentText("Open sidebar for more details.");
//                sv.show();
//                tutCounter++;
//                break;
        }
    }

    @Override
    public void onShowcaseViewShow(ShowcaseView showcaseView) {

    }

    @Override
    public void onShowcaseViewTouchBlocked(MotionEvent motionEvent) {

    }

    public void setPatternFromDialog(final PatternNode node) {
        //check node exist -> edit
        PatternNode n = patternManager.hasThisNode(node.getContactNumber());
        if(n != null){
            final SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
            pDialog.setTitleText("Are you sure?");
            pDialog.setContentText("Overwrite new pattern?");
            pDialog.setCancelable(true);
            pDialog.setCancelText("Cancel");
            pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    pDialog.cancel();
                }
            });
            pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    pDialog.dismiss();

                    //showWarning("Overwrite with new pattern?", );
                    String oldCode = node.getPatternCode();
                    node.setPatternCode(currentPattern);
                    patternManager.replaceNode(node, oldCode);
                    onRespond(oldCode, currentPattern);

                    patternFragment.setPattern("");
                    currentPattern = "";
                    contactsDialog.dismiss();
                    showResult(false, "New pattern saved.");
                }
            });
            pDialog.show();
        }else{
            node.setPatternCode(currentPattern);
            patternManager.addPatternNode(node);

            patternFragment.setPattern("");
            currentPattern = "";
            contactsDialog.dismiss();
            showResult(false, "New pattern added.");
        }
    }
}