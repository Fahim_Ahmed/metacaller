package com.thecraftybear.metacaller.interfaces;

/**
 * Created with IntelliJ IDEA.
 * User: Fahim
 * Date: 11/5/2016
 * Time: 10:34 PM
 */
public interface Communicator {
    void onRespond(String oldCode, String newCode);
    void onSuccess();
    void updateMainNumberView(int position);
    void onDrawPattern(String password);
    void onChangeFilter(int position);
}
