package com.thecraftybear.metacaller.data;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Fahim
 * Date: 10/5/2016
 * Time: 12:34 AM
 */
public class PatternNode implements Serializable{


    private String patternCode;
    private String contactNumber;
    private String contactName;

    private String _id;

    public PatternNode(){
        patternCode = "";
        contactNumber = "";
        contactName = "";
    }

    public PatternNode(String id, String patternCode, String contactNumber, String contactName) {
        this._id = id;
        this.patternCode = patternCode;
        this.contactNumber = contactNumber;
        this.contactName = contactName;
    }

    public String getPatternCode() { return patternCode; }

    public String getContactNumber() {
        return contactNumber;
    }

    public String getContactName() {
        return contactName;
    }

    public void setPatternCode(String patternCode) { this.patternCode = patternCode; }

    public void setContactNumber(String contactNumber) { this.contactNumber = contactNumber; }

    public void setContactName(String contactName) { this.contactName = contactName; }

    public String get_id() { return _id; }

    public void set_id(String _id) { this._id = _id; }
}
