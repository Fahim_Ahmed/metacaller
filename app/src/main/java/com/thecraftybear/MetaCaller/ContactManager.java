package com.thecraftybear.metacaller;

import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.ContactsContract;

import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.terrakok.phonematter.PhoneFormat;
import com.thecraftybear.metacaller.adapters.ContactListViewAdapter;
import com.thecraftybear.metacaller.data.PatternNode;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created with IntelliJ IDEA.
 * User: Fahim
 * Date: 13/5/2016
 * Time: 5:23 PM
 */


public class ContactManager{

    //final private int REQUEST_CODE_ASK_PERMISSIONS = 123;

    private final MainActivity activity;
    private ArrayList<PatternNode> contacts;
    PatternManager patternManager;

    ContactListViewAdapter listController;
    SwipeMenuListView listView;

    public ContactManager(MainActivity activity, PatternManager patternManager){
        this.activity = activity;
        this.patternManager = patternManager;

        readCachedContacts(); // return true if read from cache, else from phone memory.
    }

    public void cacheContacts(){
        try {
            FileOutputStream fos = new FileOutputStream(activity.getFilesDir() + File.separator + "contacts.code");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(contacts);
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean readCachedContacts(){
        try {
            FileInputStream fis = new FileInputStream(activity.getFilesDir() + File.separator + "contacts.code");
            ObjectInputStream ois = new ObjectInputStream(fis);
            contacts = (ArrayList<PatternNode>) ois.readObject();
            ois.close();

            System.out.println("-----> cached contacts loaded.");

        } catch (IOException | ClassNotFoundException | ClassCastException e) {
            //e.printStackTrace();
            moveContactToMain(updateContacts());
            return false;
        }

        return true;
    }

    public void refreshContacts(ContactListViewAdapter adapter){
        new RefreshContact().execute(adapter);
    }

    public PatternNode findByCode(String code){
        for (PatternNode n : contacts) {
            if (n.getPatternCode().equalsIgnoreCase(code))
                return n;
        }
        return null;
    }

    public PatternNode hasThisNode(String number){
        int len = contacts.size();
        for (int i = 0; i < len; i++) {
            PatternNode n = contacts.get(i);
            if (n.getContactNumber().equalsIgnoreCase(number))
                return n;
        }

        return null;
    }

    public PatternNode containThisName(String name){
        int len = contacts.size();
        for (int i = 0; i < len; i++) {
            PatternNode n = contacts.get(i);
            if (n.getContactNumber().contains(name))
                return n;
        }

        return null;
    }

    public ArrayList<PatternNode> updateContacts(){
        System.out.println("-----> Updating contacts");

        ArrayList<PatternNode> tmpList = new ArrayList<>();

//        if(contacts == null)
//            contacts = new ArrayList<>();
//        else contacts.clear();

        Cursor phones = activity.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);

        String prevPhoneNum = "";

        PhoneFormat pf = new PhoneFormat(activity);

        while (phones.moveToNext())
        {
            //String id = phones.getString(phones.getColumnIndex(ContactsContract.Contacts._ID));
            String id = phones.getString(phones.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));
            String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

            phoneNumber = pf.format(phoneNumber);
            phoneNumber = phoneNumber.replaceAll("\\s+", "");
            tmpList.add(new PatternNode(id, "", phoneNumber, name));
        }

        phones.close();

        Collections.sort(tmpList, new Comparator<PatternNode>() {
            @Override
            public int compare(PatternNode lhs, PatternNode rhs) {
                String nameA = lhs.getContactName().toUpperCase();
                String nameB = rhs.getContactName().toUpperCase();
                return nameA.compareTo(nameB);
            }
        });

        int len = tmpList.size();
        try {
            for (int i = 0; i < len; i++) {
                PatternNode node = tmpList.get(i);
                String num = node.getContactNumber();
                if (num.equalsIgnoreCase(prevPhoneNum)) {
                    tmpList.remove(i);
                }
                prevPhoneNum = num;
            }
        }catch (IndexOutOfBoundsException ex){
            //ex.printStackTrace();
        }

        System.out.println("-----> Done updating contacts");

        return tmpList;
    }

    // This bhonita to save execution time
    private void moveContactToMain(ArrayList<PatternNode> tmpList){
        if(contacts == null)
            contacts = new ArrayList<>();
        else contacts.clear();

        for (PatternNode node : tmpList) {
            PatternNode n = patternManager.hasThisNode(node.getContactNumber());
            //if(n!=null) n.set_id(node.get_id());
            contacts.add((n == null) ? node : n);
        }
    }

    public void setPatternManager(PatternManager patternManager){ this.patternManager = patternManager; }

    public void removeCode(String code) {
        for (PatternNode n : contacts) {
            if (n.getPatternCode().equalsIgnoreCase(code)) {
                n.setPatternCode("");
                updateListView();
                return;
            }
        }
    }

    public void cleanData() {
        for (PatternNode n : contacts) {
            if (n.getPatternCode().length() > 1)
                n.setPatternCode("");
        }

        listController.notifyDataSetChanged();
    }

    class RefreshContact extends AsyncTask<ContactListViewAdapter, Void, ContactListViewAdapter>{

        ArrayList<PatternNode> tmpList = new ArrayList<>();

        @Override
        protected final ContactListViewAdapter doInBackground(ContactListViewAdapter... params) {
            tmpList = updateContacts();
            return params[0];
        }

        @Override
        protected void onPostExecute(ContactListViewAdapter list) {
            super.onPostExecute(list);

            moveContactToMain(tmpList);
            updateListView();

            cacheContacts();
        }
    }

    public void updateListView(){
        listController.notifyDataSetChanged();
        listView.invalidate();
    }

    public void setListView(SwipeMenuListView listView) { this.listView = listView; }

    public void setListController(ContactListViewAdapter listController) { this.listController = listController; }

    public ArrayList<PatternNode> getContacts() { return contacts; }
}
